require "application_system_test_case"

class ShopsTest < ApplicationSystemTestCase
  setup do
    @shop = shops(:one)
  end

  test "visiting the index" do
    visit shops_url
    assert_selector "h1", text: "Shops"
  end

  test "creating a Shop" do
    visit shops_url
    click_on "New Shop"

    fill_in "City", with: @shop.city
    fill_in "Housenumber", with: @shop.housenumber
    fill_in "Lat", with: @shop.lat
    fill_in "Lon", with: @shop.lon
    fill_in "Name", with: @shop.name
    fill_in "Place", with: @shop.place_id
    fill_in "Street", with: @shop.street
    fill_in "Zipcode", with: @shop.zipcode
    click_on "Create Shop"

    assert_text "Shop was successfully created"
    click_on "Back"
  end

  test "updating a Shop" do
    visit shops_url
    click_on "Edit", match: :first

    fill_in "City", with: @shop.city
    fill_in "Housenumber", with: @shop.housenumber
    fill_in "Lat", with: @shop.lat
    fill_in "Lon", with: @shop.lon
    fill_in "Name", with: @shop.name
    fill_in "Place", with: @shop.place_id
    fill_in "Street", with: @shop.street
    fill_in "Zipcode", with: @shop.zipcode
    click_on "Update Shop"

    assert_text "Shop was successfully updated"
    click_on "Back"
  end

  test "destroying a Shop" do
    visit shops_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Shop was successfully destroyed"
  end
end
