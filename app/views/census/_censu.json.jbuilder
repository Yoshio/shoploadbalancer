json.extract! censu, :id, :sum, :shop_id, :created_at, :updated_at
json.url censu_url(censu, format: :json)
