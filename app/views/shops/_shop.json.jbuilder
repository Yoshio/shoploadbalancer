json.extract! shop, :id, :name, :lat, :lon, :street, :housenumber, :zipcode, :city, :place_id, :pax, :status, :state
json.url shop_url(shop, format: :json)
