json.extract! ticket, :id, :uuid, :shop_id, :appointment, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
