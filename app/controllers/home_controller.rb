class HomeController < ApplicationController
    def index
        # get bounding box from input

        unless params.has_key?(:query)
            params[:query] = "Krefeld"
        end

        info = get_info(params[:query])
        @bounding_box = info[0]["boundingbox"]
        @place_id =  info[0]["place_id"]

        osm_id = 666;

        info.each do |record|
            if record["class"] == "boundary" && record["type"] == "administrative" || record["class"] == "place" && record["type"] == "postcode"
                osm_id = record["osm_id"]
                puts "--------------"
                puts osm_id
                break
            end
        end

        area_id = 3600000000 + osm_id

        @shops = Shop.retrieve(@place_id, @bounding_box, area_id).sort_by &:pax
    end

    def get_info(query)
        require 'net/http'
        require 'json'

        url = "https://nominatim.openstreetmap.org/search/#{query}?format=json"
        begin
            uri = URI.parse(url)
          rescue URI::InvalidURIError
            uri = URI.parse(URI.escape(url))
        end
        response = Net::HTTP.get(uri)
        data = JSON.parse(response)
        
        return data
    end
end
