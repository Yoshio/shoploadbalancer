class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.all
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @census = Censu.new()

    qrcode_info = "https://shopbalancer.herokuapp.com" + url_for(shop_ticket_path(@ticket.shop, @ticket.key))
    require 'rqrcode'
    qrcode = RQRCode::QRCode.new(qrcode_info)

    # NOTE: showing with default options specified explicitly
    @svg = qrcode.as_svg(
      offset: 0,
      color: '000',
      shape_rendering: 'crispEdges',
      module_size: 6,
      standalone: true
    )
  end

  # GET /tickets/new
  def new
    @shop = Shop.find(params[:shop_id])
    @ticket = @shop.tickets.new(appointment: Time.zone.now)
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @shop = Shop.find(params[:shop_id])
    @ticket = @shop.tickets.new(ticket_params)

    respond_to do |format|
      if @ticket.save
        # refetch to get postgres's uuid (key)
        @ticket = Ticket.find(@ticket.id)
        format.html { redirect_to shop_ticket_path(@shop, @ticket.key), notice: 'Ticket erfolgreich erstellt.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.where(key: params[:id]).first
    end

    # Only allow a list of trusted parameters through.
    def ticket_params
      params.require(:ticket).permit(:shop_id, :appointment)
    end
end