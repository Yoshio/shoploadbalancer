class CensusController < ApplicationController
  before_action :set_censu, only: [:show, :edit, :update, :destroy]

  # GET /census
  # GET /census.json
  def index
    @census = Censu.all
  end

  # GET /census/1
  # GET /census/1.json
  def show
  end

  # GET /census/new
  def new
    @shop = Shop.find(params[:shop_id])
    @censu = @shop.census.new
  end

  # GET /census/1/edit
  def edit
  end

  # POST /census
  # POST /census.json
  def create
    @shop = Shop.find(params[:shop_id])
    @censu = @shop.census.new(censu_params.except(:return))

    respond_to do |format|
      if @censu.save
        format.html { redirect_to shop_ticket_path(@shop, censu_params[:return]), notice: 'Danke für die Mithilfe!' }
        format.json { render :show, status: :created, location: @censu }
      else
        format.html { render :new }
        format.json { render json: @censu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /census/1
  # PATCH/PUT /census/1.json
  def update
    respond_to do |format|
      if @censu.update(censu_params)
        format.html { redirect_to @censu, notice: 'Censu was successfully updated.' }
        format.json { render :show, status: :ok, location: @censu }
      else
        format.html { render :edit }
        format.json { render json: @censu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /census/1
  # DELETE /census/1.json
  def destroy
    @censu.destroy
    respond_to do |format|
      format.html { redirect_to census_url, notice: 'Censu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_censu
      @censu = Censu.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def censu_params
      params.require(:censu).permit(:sum, :shop_id, :return)
    end
end
