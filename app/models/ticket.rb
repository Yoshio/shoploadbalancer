class Ticket < ApplicationRecord
  belongs_to :shop

  def is_valid?
    Time.now.between?(self.appointment - 10.minutes, self.appointment + 1.hour)
  end
end
