class Shop < ApplicationRecord

    has_many :census
    has_many :tickets

    def state
        if census.exists?
            case census.last.sum
            when 0..10 then "empty"
            when 11..50 then "busy"
            else
                "full"
            end
        else
            "no_data"
        end
    end

    def status
        if census.exists?
            case census.last.sum
            when 0..10 then "green"
            when 11..50 then "yellow"
            else
                "red"
            end
        else
            "blue"
        end
    end

    def pax
        if self.census.any?
            self.census.last.sum
        end
    end

    def address
        unless self.street.nil? || self.housenumber.nil?
            self.street + " " + self.housenumber.to_s
        end
        unless self.street.nil?
            self.street
        end
    end

    def postal
        unless self.zipcode.nil? || self.city.nil?
            self.zipcode.to_s + " " + self.city
        end
    end
    
    # this will either return all the shops found, or fetch them from Overpass API first
    def self.retrieve(place_id, bounding_box, area_id)
        unless self.where(place_id: place_id).any?
            require 'overpass_api_ruby'

            options={
                :bbox => {
                    :s => bounding_box[0],
                    :w => bounding_box[2],
                    :n => bounding_box[1],
                    :e => bounding_box[3]
                },          
                :timeout => 10,
                :maxsize => 1073741824
            }
    

            overpass = OverpassAPI::QL.new(options)

            query = "
            area(id:#{area_id});
            nwr(area)[shop=supermarket];
            out center;"
    
            shops = overpass.query(query)
        
            current_shop_count = Shop.count
        
            shops[:elements].each do |shop|
                if shop.has_key?(:tags)
                    if shop[:type] == "way"
                        unless shop[:tags][:name].nil? # Guard against empty Sets
                            Shop.find_or_create_by(
                                municipal_name: shop[:display_name],
                                name: shop[:tags][:name],
                                lat: shop[:center][:lat],
                                lon: shop[:center][:lon],
                                street: shop[:tags][:"addr:street"],
                                housenumber: shop[:tags][:"addr:housenumber"],
                                zipcode: shop[:tags][:"addr:postcode"],
                                city: shop[:tags][:"addr:city"],
                                place_id: place_id
                            )
                        end
                    elsif shop[:type] == "node"
                        unless shop[:tags][:name].nil? # Guard against empty Sets
                            Shop.find_or_create_by(
                                municipal_name: shop[:display_name],
                                name: shop[:tags][:name],
                                lat: shop[:lat],
                                lon: shop[:lon],
                                street: shop[:tags][:"addr:street"],
                                housenumber: shop[:tags][:"addr:housenumber"],
                                zipcode: shop[:tags][:"addr:postcode"],
                                city: shop[:tags][:"addr:city"],
                                place_id: place_id
                            )
                        end
                    end
                end
            end
        
            p "Added #{Shop.count - current_shop_count} new shops"
        end
        self.where(place_id: place_id)
    end
end
