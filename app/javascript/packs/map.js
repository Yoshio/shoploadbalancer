document.addEventListener("turbolinks:load", () => {

  if (!document.querySelector("#map"))
    return
    
  console.log("activate map");

  var map = L.map('map');

  // Krefeld
  /*
  var corner1 = L.latLng(51.251601468176545, 6.45069122314453); 
  var corner2 = L.latLng(51.439670742972, 6.733589172363281);
  var bounds = L.latLngBounds(corner1, corner2);

  map.fitBounds(bounds);
  */

  const bounds = JSON.parse(document.getElementById("map").getAttribute("data-bounds"));
  const place_id = JSON.parse(document.getElementById("map").getAttribute("data-place_id"));

  map.fitBounds(bounds);

  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: osm_key
  }).addTo(map);

  var request = new XMLHttpRequest();
  request.open('GET', '/shops.json?place_id=' + place_id, true);

  request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
      // Success!
      var data = JSON.parse(request.responseText);

      const Pins = L.Icon.extend({
        options: {
            shadowUrl: '/markers/shadow.png',
            iconSize: [35, 45],
            iconAnchor:   [17, 42],
            popupAnchor: [1, -32],
            shadowAnchor: [10, 12],
            shadowSize: [36, 16]
        }
      });
      
      var redMarker = new Pins({iconUrl: '/markers/red.png'});
      var yellowMarker = new Pins({iconUrl: '/markers/yellow.png'});
      var greenMarker = new Pins({iconUrl: '/markers/green.png'});
      var blueMarker = new Pins({iconUrl: '/markers/blue.png'});

      for (var k in data) {
          var marker = blueMarker;
          switch(data[k].state) {
            case "full":
              marker = redMarker;
              break;
            case "busy":
              marker = yellowMarker;
              break;
            case "empty":
              marker = greenMarker;
              break;
            default:
              marker = blueMarker;
          }
          L.marker([data[k].lat, data[k].lon], {icon: marker}).addTo(map);
          /*
          L.circle([data[k].lat, data[k].lon], {
              color: data[k].status,
              fillColor: '#f03',
              fillOpacity: 0.5,
              radius: 100
          }).addTo(map);
          */
      }
  } else {
      // We reached our target server, but it returned an error
  }
  };

  request.onerror = function() {
  // There was a connection error of some sort
  };

  request.send();
});