# #WirVsVirus Hackathon

**1_17_b_supermarkt_status_ShopLoadBalancer**

[devpost](https://devpost.com/software/shoploadbalancer)

Demo auf : [shopbalancer.herokuapp.com](https://shopbalancer.herokuapp.com/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/CGit9S_WEqY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Technisches:
Die aktuelle Auslastung eines Supermarktes wird derzeit - zwecks veranschaulichung - alle 10min Randomisiert.

Es ist aber bereits möglich, auf der Ticket (QR Code) Seite den aktuellen Stand zu übermitteln (Crowd Sourcing)

Des weiteren ist eine einfache Anbindung an die REST Api möglich, um bsp. bereits vorhandene Fluktuationsmessungen aus einem Supermarkt zu empfangen.

## LICENSE
Muss noch nachgearbeitet werden!

Das meisten Dependencies sind aber MIT