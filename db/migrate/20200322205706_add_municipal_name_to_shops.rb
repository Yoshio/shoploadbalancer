class AddMunicipalNameToShops < ActiveRecord::Migration[6.0]
  def change
    add_column :shops, :municipal_name, :string
  end
end
