class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'uuid-ossp'
    create_table :tickets do |t|
      t.uuid :key, null: false, default: 'uuid_generate_v4()'
      t.integer :uuid
      t.references :shop, null: false, foreign_key: true
      t.datetime :appointment

      t.timestamps
    end
  end
end
