class CreateCensus < ActiveRecord::Migration[6.0]
  def change
    create_table :census do |t|
      t.integer :sum
      t.references :shop, null: false, foreign_key: true

      t.timestamps
    end
  end
end
