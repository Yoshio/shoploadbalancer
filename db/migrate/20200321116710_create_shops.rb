class CreateShops < ActiveRecord::Migration[6.0]
  def change
    create_table :shops do |t|
      t.string :name
      t.float :lat
      t.float :lon
      t.string :street
      t.integer :housenumber
      t.integer :zipcode
      t.string :city
      t.integer :place_id

      t.timestamps
    end
  end
end
